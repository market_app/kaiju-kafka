"""
This module contains basic kafka producer and consumer services.
"""

import abc
import asyncio
import uuid
from typing import Optional

import aiokafka

from kaiju_tools.services import ContextableService
from kaiju_tools.serialization import dumps_bytes, loads

from .abc import AbstractKafkaProducerService, AbstractKafkaConsumerService

__all__ = (
    'BaseKafkaService', 'KafkaProducerService', 'KafkaConsumerService'
)


class BaseKafkaService(ContextableService, abc.ABC):
    """
    Base kafka service.

    :param app: web app
    :param conn_settings: kafka transport settings
    :param logger: optional logger instance
    """

    kafka_transport_class = None

    def __init__(self, app, conn_settings: dict, loop=None, logger=None):
        super().__init__(app=app, logger=logger)
        self._prepare_conn_settings(conn_settings)
        self._conn_settings = conn_settings
        self._loop = loop
        self._transport = self._create_transport()

    def _prepare_conn_settings(self, conn_settings: dict):
        if 'client_id' not in conn_settings:
            if self.app:
                conn_settings['client_id'] = str(self.app.id)
            else:
                conn_settings['client_id'] = uuid.uuid4()

    def _create_transport(self) -> kafka_transport_class:
        transport = self.kafka_transport_class(
            loop=self._get_loop(), **self._conn_settings)
        return transport

    @property
    def transport(self):
        return self._transport

    async def init(self):
        await self._transport.start()

    async def close(self):
        await self._transport.stop()

    @property
    def closed(self) -> bool:
        return self._transport is None or self._transport._closed

    def _get_loop(self):
        if self.app:
            return self.app.loop
        elif self._loop:
            return self._loop
        else:
            _loop = asyncio.get_running_loop()
            if _loop:
                return _loop
            else:
                return asyncio.get_event_loop()

    def __getattr__(self, item):
        return getattr(self.transport, item)


class KafkaProducerService(BaseKafkaService, AbstractKafkaProducerService):
    """
    Kafka producer.

    You may access all :class:`aiokafka.AIOKafkaProducer` methods directly from
    this service.

    :param app: web app
    :param conn_settings: kafka transport settings
    :param logger: optional logger instance
    """

    kafka_transport_class = aiokafka.AIOKafkaProducer

    def _prepare_conn_settings(self, conn_settings: dict):
        super()._prepare_conn_settings(conn_settings)
        conn_settings['value_serializer'] = dumps_bytes


class KafkaConsumerService(BaseKafkaService, AbstractKafkaConsumerService):
    """
    Kafka consumer.

    You may access all :class:`aiokafka.AIOKafkaConsumer` methods directly from
    this service.

    :param app: web app
    :param conn_settings: kafka transport settings
    :param logger: optional logger instance
    """

    kafka_transport_class = aiokafka.AIOKafkaConsumer

    def _prepare_conn_settings(self, conn_settings: dict):
        super()._prepare_conn_settings(conn_settings)
        conn_settings['value_deserializer'] = loads

    def _create_transport(self) -> kafka_transport_class:
        transport = self.kafka_transport_class(
            *self._conn_settings.pop('topics'),
            loop=self._get_loop(), **self._conn_settings)
        return transport
