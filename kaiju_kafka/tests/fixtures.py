"""
Place your pytest fixtures here.
"""

import uuid
import pytest

from kaiju_tools.tests.fixtures import *

from ..services import KafkaProducerService, KafkaConsumerService


KAFKA_HOST = 'localhost'
ZK_PORT = '2181'
KAFKA_PORT = '9092'
TEST_TOPIC = str(uuid.uuid4())


def _kafka_container(container_function):
    return container_function(
        image='johnnypark/kafka-zookeeper',
        name='kafka',
        version='latest',
        ports={'9092': KAFKA_PORT, '2181': ZK_PORT},
        env={
            'ADVERTISED_HOST': KAFKA_HOST,
            'ADVERTISED_PORT': KAFKA_PORT
        },
        sleep=12,
        healthcheck={
            'test': "/opt/kafka_2.12-2.4.0/bin/kafka-topics.sh --list --zookeeper=localhost:2181",
            'interval': 100000000,
            'timeout': 6000000000,
            'start_period': 5000000000,
            'retries': 3
        }
    )


@pytest.fixture
def kafka(container):
    """
    Returns a new kafka/zk container. See `kaiju_tools.tests.fixtures.container`
    for more info.
    """

    return _kafka_container(container)


@pytest.fixture(scope='session')
def per_session_kafka(per_session_container):
    """
    Returns a new kafka container. See `kaiju_tools.tests.fixtures.per_session_container`
    for more info.
    """

    return _kafka_container(per_session_container)


@pytest.fixture
def kafka_producer(logger, loop):
    producer = KafkaProducerService(
        app=None,
        loop=loop,
        conn_settings={
            'bootstrap_servers': f'{KAFKA_HOST}:{KAFKA_PORT}'
        },
        logger=logger
    )
    return producer


@pytest.fixture
def kafka_consumer(logger, loop):
    consumer = KafkaConsumerService(
        app=None,
        loop=loop,
        conn_settings={
            'topics': [TEST_TOPIC],
            'bootstrap_servers': f'{KAFKA_HOST}:{KAFKA_PORT}',
            'auto_offset_reset': 'earliest'
        },
        logger=logger
    )
    return consumer
