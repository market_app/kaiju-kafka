from .fixtures import *


async def test_kafka_connectivity(per_session_kafka, kafka_producer, kafka_consumer, logger):

    logger.info('Testing producer.')

    async with kafka_producer:
        msg = {'some': 'message'}
        logger.info('Sending a single message...')
        result = await kafka_producer.send_and_wait(TEST_TOPIC, msg, partition=0)
        logger.debug(result)

    logger.info('Testing consumer.')

    async with kafka_consumer:
        logger.info('Receiving a single message...')
        async for _msg in kafka_consumer.transport:
            logger.debug(_msg)
            assert _msg.value == msg
            break
