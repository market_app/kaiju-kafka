"""
Abstract or base classes and interfaces. May be used for type hinting.
"""

import abc

__all__ = ('AbstractKafkaProducerService', 'AbstractKafkaConsumerService')


class AbstractKafkaProducerService(abc.ABC):
    service_name = 'kafka_producer'


class AbstractKafkaConsumerService(abc.ABC):
    service_name = 'kafka_consumer'
